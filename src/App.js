import React from 'react';
import './App.css';

class App extends React.Component {
    state = {
        data: []
    };

    componentDidMount() {
        fetch('https://bitbucket.org/GuestOne/goodline-test-task/raw/aa16bc87df8355deef678f4b69a2b6d9fe0f9832/frontend/data.json',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            .then(response => response.json())
            .then(dataResponse => {
                this.setState({
                    data: dataResponse
                });
            });
    }

    render() {
        const rowTable = this.state.data.map(item => (
            <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.age}</td>
                <td>{item.phone}</td>
            </tr>
        ));

        return (
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Phone</th>
                    </tr>
                </thead>
                <tbody>
                    {rowTable}
                </tbody>
            </table>
        );
    }
}

export default App;
